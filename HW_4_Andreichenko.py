import re

# I
# Есть строка произвольного содержания.
# Написать код, который найдет в строке самое короткое слово, в котором присутствуют подряд две гласные буквы.

# II
# Есть два числа - минимальная цена и максимальная цена.
# Дан словарь продавцов и цен на какой то товар у разных продавцов:
# Написать код, который найдет и выведет на экран список продавцов,
# чьи цены попадают в диапазон между нижней и верхней ценой.


# I implementation
def shortestWordWithTwoVowels():
    match_lst = [
        "aa", "uu", "ii", "oo", "ee",
        "au","ua", "iu", "ui", "ou", "uo", "eu", "ue",
        "ai", "ia", "oi", "io", "ei", "ie",
        "ao", "oa", "eo", "oe",
        "ae", "ea"
    ]
    print("Input text: ")
    text = input()

    split_list = text.split()
    shortest = min(split_list, key=len)

    while split_list:

        for symb in match_lst:
            if re.findall(symb, shortest.lower()):
                print("The shortest word with two sequent vowels: ", shortest)
                return

        if split_list:
            split_list.remove(shortest)
            if split_list:
                shortest = min(split_list, key=len)

    print("There is no word with two sequent vowels.")


# II implementation
def diapason():

    dict = {
        "it-proger": 22.5, "oshadbank": 34.7, "nova.net": 35.7,
        "g-store": 37.166, "royal-service": 37.245, "sota": 37.720,
        "buy.ua": 38.324,  "ipartner": 38.988, "istudio": 42.999,
        "result.com": 44.5, "citrus": 47.999, "moyo": 49.549,
        "rozetka": 51.003, "privat": 53.1, "mihoyo": 55.3
    }

    match = ""
    val_lst = list(dict.values())
    key_lst = list(dict.keys())

    print("Set lower and upper limits (can be whole or decimal (with dot) number):")
    print("Lower: ", end="")
    lower_limit = input()
    print("Upper: ", end="")
    upper_limit = input()

    try:
        lower_limit = float(lower_limit)
        upper_limit = float(upper_limit)
    except:
        print("Wrong Input!")
        return



    for value in val_lst:
        if  lower_limit <= value <= upper_limit:
           pos = val_lst.index(value)
           match = match + str(key_lst[pos]) + "    "

    if match != "":
        print("Found matches: ", match)
    else:
        print("No matches")

# main main main main main main main main
def main():
    shortestWordWithTwoVowels()
    #diapason()


main()